package Magda.ProgramTree.Expressions;


public class CBinaryExpressionLess extends CBinaryExpression
{	
	public CBinaryExpressionLess (IExpression aMethodTarget, IExpression param)
	{ super( aMethodTarget, "Comparable", "less", param);
	}

};