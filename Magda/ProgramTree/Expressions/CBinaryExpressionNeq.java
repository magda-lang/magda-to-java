package Magda.ProgramTree.Expressions;


public class CBinaryExpressionNeq extends CBinaryExpression
{
  public CBinaryExpressionNeq (IExpression aMethodTarget, IExpression param)
  { super( aMethodTarget, "Object", "neq", param);
  }

};