package Magda.mtj;

public abstract  class CMagdaMethod implements IMagdaObjectElement
{
	public abstract CMagdaObject Execute(CMagdaObject aSelf, CMagdaObject[] params);
};
