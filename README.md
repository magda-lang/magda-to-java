# The Magda language Overview

Magda is a programming language introduced in Jarek Kusmierek's PhD
(http://www.mimuw.edu.pl/~jdk/mixiny.pdf) and continued in Mauro
Mulatero's thesis
(http://www.tesionline.it/default/tesi.asp?idt=45612).

Magda's goal is to allow a programmer to write well-modularized,
reusable code.

Magda is based upon the core notion of mixin as the only unit of
reuse. The power of modularization of mixins is enhanced by two unique
features.

The first feature is the modularization of constructors. In Magda,
many mixins with independent definitions of constructors can be
combined without the need to copy any code and without the risk of
clashes.

The second distinctive feature modifies the way declarations of new
methods, overriding methods, and method calls are done, in order to
rule out accidental name clashes, even with respect to future
modifications of the code.

Samples written in Magda language, together with an IDE (by Marco
Naddeo), are available in the download.

## Textpad plugin

With this Magda package, the "magda.syn" file is available in the path
"\mtj\Textpad syn plugin".  This file allow the user to setup the
editor TextPad (http://www.textpad.com/) with the Magda syntax
highlight.  Refer to the TextPad help for syn file usage.

## Usage

No installations are needed. In order to use the Magda language simply
place this "mtj" tree in the computer media and call "compile" and
"execute" as explained later.  This Magda language prototype uses Java
classes (java 1.5) included inside this "mtj" package and no specific
configurations for Magda are needed; anyway is necessary that the pc
used contains the Java Sun JDK platform installed and working.

Just as an example main environment variable should be something like:
```
rem rem Copyright 2004-2005 Sun Microsystems, Inc. All rights
reserved.  rem Use is subject to license terms.  rem

set AS_ANT=C:\Sun\AppServer\lib\ant set
AS_ANT_LIB=C:\Sun\AppServer\lib\ant\lib set
AS_POINTBASE=C:\Sun\AppServer\pointbase set
AS_POINTBASE_SAMPLESDB=C:\Sun\AppServer\pointbase set
AS_WEBSERVICES_LIB=C:\Sun\AppServer\lib set
AS_PERL=C:\Sun\AppServer\lib\perl set AS_NSS=C:\Sun\AppServer\lib set
AS_NSS_BIN=C:\Sun\AppServer\lib set
AS_IMQ_LIB=C:\Sun\AppServer\imq\lib set
AS_IMQ_BIN=C:\Sun\AppServer\imq\bin set
AS_CONFIG=C:\Sun\AppServer\config set AS_INSTALL=C:\Sun\AppServer set
AS_JAVA=C:\Sun\AppServer\jdk set
AS_ACC_CONFIG=C:\Sun\AppServer\domains\domain1\config\sun-acc.xml set
AS_JHELP=C:\Sun\AppServer\lib set AS_ICU_LIB=C:\Sun\AppServer\bin set
AS_DEF_DOMAINS_PATH=C:\Sun\AppServer\domains set
AS_JDMK_HOME=C:\Sun\AppServer\lib\SUNWjdmk\5.1 set
AS_NATIVE_LAUNCHER=true set
AS_NATIVE_LAUNCHER_LIB_PREFIX=\jre\bin\client set
AS_WEBCONSOLE_LIB=C:\Sun\AppServer\lib set
AS_JATO_LIB=C:\Sun\AppServer\lib set AS_HADB=%HADB_HOME%

set CLASSPATH=%CLASSPATH%;.

path = C:\Sun\AppServer\jdk\bin;%path%
```
In this example the Java Sun JDK is installed in C:\


This Magda milestone is built and checked on a Windows XP based
machine.  Maybe is possible to test the language also in other
O.S. with little modifications at the script level (not available at
the moment).

All sample user programs are present (and must be) in the path
\mtj\Magda\src\...

To compile a program, from command prompt, type: compile ProgramName
  Note: ProgramName without extension

Example: to compile the program MagdaTest.magda in \mtj type: compile
  MagdaTest
  
To execute a program, from command prompt, type: execute ProgramName
  Note: ProgramName without extension

Example: to execute the program MagdaTest.magda in \mtj type: execute
  MagdaTest

NOTES: Any new program must be created in a folder with the same name
       of the Magda program that contains the 'main' and must be
       placed inside the path: \mtj\Magda\src\...  With 'main' we
       intend the method that starts the computation, but this is not
       necessarily called "main", as in Magda does not exist an
       "official" main method.

       The program folder must contain a sub-folder named 'obj' that needs to
       be created (empty) before compiling.

       All sample user program are present (and must be) in the path \mtj\Magda\src\...
       
       To rebuild the Magda language, if necessary and from command
       prompt, type: make
