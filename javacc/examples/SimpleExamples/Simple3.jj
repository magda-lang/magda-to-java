
/*
 * Copyright © 2002 Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * California 95054, U.S.A. All rights reserved.  Sun Microsystems, Inc. has
 * intellectual property rights relating to technology embodied in the product
 * that is described in this document. In particular, and without limitation,
 * these intellectual property rights may include one or more of the U.S.
 * patents listed at http://www.sun.com/patents and one or more additional
 * patents or pending patent applications in the U.S. and in other countries.
 * U.S. Government Rights - Commercial software. Government users are subject
 * to the Sun Microsystems, Inc. standard license agreement and applicable
 * provisions of the FAR and its supplements.  Use is subject to license terms.
 * Sun,  Sun Microsystems,  the Sun logo and  Java are trademarks or registered
 * trademarks of Sun Microsystems, Inc. in the U.S. and other countries.  This
 * product is covered and controlled by U.S. Export Control laws and may be
 * subject to the export or import laws in other countries.  Nuclear, missile,
 * chemical biological weapons or nuclear maritime end uses or end users,
 * whether direct or indirect, are strictly prohibited.  Export or reexport
 * to countries subject to U.S. embargo or to entities identified on U.S.
 * export exclusion lists, including, but not limited to, the denied persons
 * and specially designated nationals lists is strictly prohibited.
 */

options {DEBUG_PARSER = false;}


PARSER_BEGIN(Simple3)

public class Simple3 {
  

  public static void main(String args[]) throws ParseException {
    System.out.println("Launching magda compiler");
    Simple3 parser = new Simple3(System.in);
    parser.Program();
//    parser.Input();
    System.out.println("Program successfully checked");
  }

}

PARSER_END(Simple3)


SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
}

TOKEN :
{
  <REPL: "repl">
| <WHILE: "while">
| <THIS: "this">
| <BEGIN: "begin">
| <SUPER:"super">
| <NEW: "new"> 
| <ABSTRACT: "abstract"> 
| <OVERRIDE: "override">
| <IF: ("If" |"if")>
| <PARAM: "param">
| <REQ: "req">
| <END: "end">
| <OF: "of">
| <LET: "let">
| <MIXIN: "mixin">
| <THEN: "then">
| <INSTR: "instr">
| <LBRACE: "{">
| <RBRACE: "}">
| < ID: ["a"-"z","A"-"Z","_"] ( ["a"-"z","A"-"Z","_","0"-"9"] )* >
| < INTEGER_LITERAL: (["0"-"9"])+ >

}

int SomethingElse():
{  Token t;
}
{ ( <INSTR> 
   {return 10;} ) |
  ( t= <INTEGER_LITERAL> )
  { return Integer.parseInt(t.image);
  }
}

int Something():
{ int t1, t2;}
{ <IF> t1= SomethingElse() <THEN> t2=SomethingElse() "end;"
  {return t1*t2;}
}

void Input() :
{ int count; }
{
  (count=MatchedBraces() | count=Something() ) <EOF>
  { System.out.println("The levels of nesting is " + count); }
}

int MatchedBraces() :
{ int nested_count=0; }
{
  <LBRACE> [ nested_count=MatchedBraces() ] <RBRACE>
  { return nested_count+1; }
}

/* --------------------------------------------------*/
/* -------------- Here starts Magda Grammar ---------*/
/* --------------------------------------------------*/


void FieldAssignment():
{}
{ <THIS> "." <ID> "." <ID> ":=" Expression() 
}

void WhileLoop():
{}
{ <WHILE> "(" Expression() ")" <END> 
}

void VariableAssignment():
{}
{  <ID> ":=" Expression() 
}

void Instruction():
{}
{ ( LOOKAHEAD(2) Expression() |
    VariableAssignment() |
    FieldAssignment() |
    WhileLoop() 
  ) ";"
}

void ActualParameters():
{}
{ "(" [ Expression () ("," Expression() )* ] ")"
}


void MethodCallSuffix ():
{ Token MethodMixin, MethodName; }
{  "." MethodMixin = <ID> "." MethodName = <ID> ActualParameters() 
  { System.out.println ("Call of method "+MethodMixin.image+ "/"+ MethodName.image);}
}

void Expression():
{}
{ (
   <THIS> |
   <ID> |
   "(" Expression() ")" |
   ObjectCreation() 
  )  ( MethodCallSuffix () )*
}

void ActualInitializationOfOneParam():
{}
{ <ID> ":=" Expression()
}

void ActualInitializationOfParams():
{}
{ "(" [ActualInitializationOfOneParam() ("," ActualInitializationOfOneParam())*] ")"
}

void ObjectCreation():
{}
{ <NEW> <ID> ActualInitializationOfParams() 
}

void Declaration():
{}
{ ( Mixin() |
    LetDeclaration()
  ) ";"
}

/*void MixinExpressionContinuation():
{}
{ 
  ("," <ID> |
   "(" MixinExpression() ")"
  )*
}

void MixinExpression():
{}
{ ("(" MixinExpression() ")") |
  <ID>  MixinExpressionContinuation() 
}*/

void MixinExpression():
{}
{ 
  "(" MixinExpression() ")" |
  <ID> (","  (<ID> | "(" MixinExpression() ")") )*
}


void FieldDeclaration():
{}
{ <ID> ":" MixinExpression() 
}

void BaseRule():
{}
{ <REQ> <ID> 
}

void ParameterDeclaration():
{}
{ <PARAM> <ID> ":" MixinExpression() 
}

void ReplacementRule():
{}
{ <REPL> <ID> "<-" <ID> ("," <ID>)*
}

void ParameterDecl():
{}
{ <ID> ":" MixinExpression()
}


void ParametersDecl():
{}
{ "(" 
[ParameterDecl() (";" ParameterDecl())*]
  ")"
}

void localVariablesDeclaration():
{}
{ (<ID> ":" MixinExpression() ";")*
}

void MethodBody():
{}
{ ( localVariablesDeclaration() )
  <BEGIN>
  (Instruction() )*
  <END>
}

void MethodDeclaration():
{}
{ <ABSTRACT> MixinExpression() <ID> ParametersDecl()  |
  <NEW> MixinExpression() <ID> ParametersDecl() MethodBody() |
  <OVERRIDE> MixinExpression() <ID> "." <ID>  ParametersDecl()  MethodBody() 
}

void Mixin():
{}
{ <MIXIN> <ID> "of" [ MixinExpression() ] "="
    ( (FieldDeclaration()|
       MethodDeclaration()|
       ParameterDeclaration() |
       BaseRule()|
       ReplacementRule()
       )* ";"
    )
  <END> 
}

void LetDeclaration():
{}
{ <LET> <ID> "=" MixinExpression() 
}

void Program():
{ Object m1;}
{ ( Declaration() {} ) *
    Instruction()
}